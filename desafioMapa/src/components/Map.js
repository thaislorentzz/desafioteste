import React, { useState } from "react";
import Geocode from "react-geocode";
import styled from 'styled-components'
import { FaSearch } from 'react-icons/fa';


function Mapa() {
  const [cep, setCep] = useState();
  const [complemento, setComplemento] = useState();
  const [latitude, setLatitude] = useState(0);
  const [longitude, setLongitude] = useState(0);
  const [enderecoCompleto, setEnderecoCompleto] = useState();

  const handleSubmit = async (event) => {
    event.preventDefault();
    Geocode.setApiKey(process.env.REACT_APP_APIKEY);
    Geocode.setLanguage("pt-br");
    Geocode.setRegion("br");

    Geocode.fromAddress(`${cep} ${complemento}`).then(
      (response) => {
        const address = response.results[0];

        console.log(address);

        setLatitude(address.geometry.location.lat);
        setLongitude(address.geometry.location.lng);
        setEnderecoCompleto(address.formatted_address);

      },
      (error) => {
        console.error(error);
      }
    );
  }

  return (
    <Container>
      <Title>Busca Cep</Title>
      <form onSubmit={handleSubmit}>
        <input
          id="cep"
          type="number"
          placeholder="cep"
          size="20"
          value={cep}
          onChange={(e) => setCep(e.target.value)}
        />
        <input
          id="complemento"
          type="number"
          placeholder="complemento"
          size="10"
          value={complemento}
          onChange={(e) => setComplemento(e.target.value)}
        />
        <Button type='submit'><FaSearch /></Button>
        <Input
          type="text"
          placeholder="Endereço completo"
          size="46"
          disabled
          value={enderecoCompleto}
        />
      </form>
      <img src={`https://maps.googleapis.com/maps/api/staticmap?center=${latitude},${longitude}&zoom=14&size=300x300&key=${process.env.REACT_APP_APIKEY}`}
      style={{ borderRadius: 5 }} alt="mapa"/>
    </Container>
  );
}

export default Mapa;

const Title = styled.h1`
color:#3895D3;
margin-top:20px;
font-family: 'Raleway', sans-serif;
`

const Input = styled.input`
border: 1px solid #a6a6a6;
padding: 5px;
border-radius: 3px;
margin-top: 25px;
margin:5px;
color: #6c6c6c;
`

const Container = styled.div`
width: 360px;
padding: 8% 0 0;
margin: auto;
position: relative;
z-index: 1;
background: #FFFFFF;
max-width: 360px;
margin: 0 auto 100px;
padding: 45px;
text-align: center;
box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
`

const Button = styled.button`
  padding: 3px;
`