import React from 'react'
import Map from './Map';
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

  describe('Componente mapa', () => {

    test('deve iniciar com o estado vazio', () => {
      const component = shallow(<Map />)

      expect(component).toEqual({})
      expect(component.cep).toBe(undefined)
      expect(component.complemento).toEqual(undefined)
      component.unmount()
    })

    test('deve definir o valor cep', () => {
      const container = shallow(<Map />)
      container.find('#cep').simulate('change', {
        target: {
          value: '32340060',
        },
      });
      expect(container.find('#cep').prop('value')).toEqual(
        '32340060',
      );
    });

    test('deve definir o valor complemento', () => {
      const container = shallow(<Map />)
      container.find('#complemento').simulate('change', {
        target: {
          value: '163',
        },
      });
      expect(container.find('#complemento').prop('value')).toEqual(
        '163',
      );
    });

    test('deve chamar handleSubmit ao enviar o formulário', () => {
      const event = { preventDefault: () => {} };
      jest.spyOn(event, 'preventDefault');
      const component = shallow(<Map />)
      component.find('form').simulate('submit', event);
      expect(event.preventDefault).toHaveBeenCalled();
  }); 
})

